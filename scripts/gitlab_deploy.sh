#!/bin/bash

echo "Installing deployment dependencies"

apt-get update -qy

apt-get -y install zip unzip

pip install awscli