#!/bin/bash
echo "Packaging application"

zip -r ./${CI_COMMIT_SHORT_SHA}.zip *

aws s3 cp ./${CI_COMMIT_SHORT_SHA}.zip s3://${AWS_S3_BUCKET}/builds/${CI_COMMIT_SHORT_SHA}.zip

echo "Deploying package"

aws deploy create-deployment \
  --application-name node-media-server-cd-app \
  --deployment-config-name CodeDeployDefault.OneAtATime \
  --deployment-group-name production \
  --s3-location bucket=${AWS_S3_BUCKET},bundleType=zip,key=builds/${CI_COMMIT_SHORT_SHA}.zip \
  --file-exists-behavior=OVERWRITE