#!/bin/bash
sudo yum update -y

cd /home/ec2-user

echo "Install code deploy agent"
sudo yum install ruby -y
sudo yum install wget -y
aws s3 cp 's3://node-media-server/latest/install' . --region us-east-1
chmod +x ./install
sudo ./install auto
mkdir -p /home/ec2-user/app
sudo chown -R ec2-user /home/ec2-user/app

echo "Install GIT"
sudo yum install git -y

echo "Install NodeJS"
sudo yum install -y gcc-c++ make
curl -sL https://rpm.nodesource.com/setup_12.x | sudo -E bash -
sudo yum install -y nodejs

echo "Install YARN"
sudo npm install yarn -g

echo "Install FFMPEG"
wget https://johnvansickle.com/ffmpeg/releases/ffmpeg-release-amd64-static.tar.xz
tar -xf ffmpeg-release-amd64-static.tar.xz
sudo mkdir -p /usr/local/bin/ffmpeg
sudo mv ./ffmpeg-4.2.2-amd64-static/* /usr/local/bin/ffmpeg
sudo ln -s /usr/local/bin/ffmpeg/ffmpeg /usr/bin/ffmpeg

export FFMPEG_PATH=/usr/bin/ffmpeg
