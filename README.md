1. Create Launch Template
2. Create Load Balancer
3. Create Target Groups
4. Create Auto Scaling Group
5. Create Scale Up and Down Policy
6. Create Notification

# Configure CodeDeploy Agent

```
aws s3 cp --recursive s3://aws-codedeploy-us-east-1 s3://node-media-server --region=us-east-1
```

# Structure for deployment zip

```
scripts
app
appspec.yml
```

```
https://docs.aws.amazon.com/codedeploy/latest/userguide/tutorials-auto-scaling-group-create-deployment.html
```

# CodeDeploy Setup

1. Create Application - node-media-server-cd-app
2. Create Deployment Group - node-media-server-cd-group
3. Create CodeDeploy Service Role - https://docs.aws.amazon.com/codedeploy/latest/userguide/getting-started-create-service-role.html
4. Create Deployment

# Roles

1. GITLAB - Access to S3, Access to CodeDeploy
2. INSTANCE - Access to S3, Access to CodeDeploy
3. CODEDEPLOY - Access to S3, Access to CodeDeploy, Access to EC2 and ASG
